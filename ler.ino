/**
 * Ideas:
 *  - detect colision with accelerometer and then say: "Auch!!", or "WTF is that?"
 */

#include <Servo.h>

const int ACTION_FREE_ROAM = 10;
const int ACTION_WONDER    = 20;
const int ACTION_DANCE     = 30;
const int ACTION_DEFAULT   = ACTION_FREE_ROAM;

const int DIRECTION_FWD       = 1;
const int DIRECTION_RIGHT_90  = 90;
const int DIRECTION_LEFT_90   = -90;
const int DIRECTION_RIGHT_45  = 45;
const int DIRECTION_LEFT_45   = -45;
const int DIRECTION_BWD       = 180;

const int DISTANCE_MIN  = 37; // cm

// Left motor pins
const int LMotorEn = 6;
const int LMotorFw = 7;
const int LMotorBw = 8;

// Right motor pins
const int RMotorEn = 5;
const int RMotorFw = 2;
const int RMotorBw = 4;

// Servo pin
const int servoPin = 9;

// Ultrasonic sensor pin
const int pingPin = 12;

// Global vars
int LMotorSpeed = 150; // motor speed / 51 = motor voltage, if 255=5V
int RMotorSpeed = 150;
int robotIsMoving = 0;
int obstF = 0; // fwd
int obstL = 0; // left
int obstR = 0; // right

Servo myServo;


void setup() {
  // Define morot pins
  pinMode(LMotorEn, OUTPUT);
  pinMode(LMotorFw, OUTPUT);
  pinMode(LMotorBw, OUTPUT);
  pinMode(RMotorEn, OUTPUT);
  pinMode(RMotorFw, OUTPUT);
  pinMode(RMotorBw, OUTPUT);

  // Define servo pin
  pinMode(servoPin, OUTPUT);
  myServo.attach(servoPin);

  Serial.begin(9600);
}


void loop() {
  // put your main code here, to run repeatedly:
  //int act;
  //act = decideAction();
  // doAction(act);

  actionFreeRoam();
}

/* To use later

int decideAction() {
  int act;
  act = random(1,100);
  // These ranges are definde so that we can
  if (act >= 1 && act < 80) {
    return ACTION_FREE_ROAM;
  }
  else if(act >= 80 && act < 90) {
    return ACTION_WONDER;
  }
  else if(act <= 90 && act <= 100) {
    return ACTION_DANCE;
  }
}

void doAction(act) {
  switch (act) {
    case ACTION_FREE_ROAM:
      actionFreeRoam();
      break;
    case ACTION_WONDER:
      actionWonder();
      break;
    case ACTION_DANCE:
      actionDance();
    default:
      actionFreeRoam();
      break;
  }
}
*/

/**
 * In this action robot is exploring enviroment by driving around avoiding obsticles.
 * Robot can decide on random when to stop this behaviour.
 * 
 * TODO: explain loginc how free roam works..
 */
void actionFreeRoam() {

  // TODO: wrap this in while loop which can exit if random "uvjet" is met.
  
  int distances[3];
  lookAround(distances);

  Serial.print("Distances: ");
  Serial.print(distances[0]);
  Serial.println();
  Serial.print(distances[1]);
  Serial.println();
  Serial.print(distances[2]);
  Serial.println();
  
  int direc = decideDirection(distances);
  Serial.print("Direction: ");
  Serial.print(direc);
  Serial.println();

  goDirection(direc);
}

/**
 * Look forward, left and right, and return array of distances from each direction.
 * If robotIsMoving is set to 1 then robot will stop on first obsticle he finds.
 */
void lookAround(int distances[]) {
  int distFwd;
  int distLeft;
  int distRight;

  // Turn head fwd
  headFwd();
  delay(300);

  // Read distance from obsticle (if any)
  distFwd = getDistance();

  // If robot is moving and minimal distance is reached, then stop robot to avoid colision.
  if (robotIsMoving == 1 && distFwd <= DISTANCE_MIN) {
    goStop();
    // Here I just continue to make measurments (from left and right side) because robot has stopped
    // and he will stay on this position until function finishes all measurements.
  }
  
  // Turn head left
  headLeft();
  delay(300);

  // Read distance from obsticle (if any)
  distLeft = getDistance();

  // If robot is moving and minimal distance is reached, then stop robot to avoid colision.
  if (robotIsMoving == 1 && distLeft <= DISTANCE_MIN) {
    goStop();
    delay(200);
    lookAround(distances); // Here I recursively call lookAround() function again to make accurate measurements again (but this time robot will not be moving)
    return;
  }

  // Turn head right
  headRight();
  delay(370); // Here servo has tu turn for 140 degrees (from full left to full right) so it needs more time to turn.

  // Read distance from obsticle (if any)
  distRight = getDistance();

  // If robot is moving and minimal distance is reached, then stop robot to avoid colision.
  if (robotIsMoving == 1 && distRight <= DISTANCE_MIN) {
    goStop();
    delay(200);
    lookAround(distances); // Here I recursively call lookAround() function again to make accurate measurements again (but this time robot will not be moving)
    return;
  }

  distances[0] = distFwd;
  distances[1] = distLeft;
  distances[2] = distRight;

  // Turn head fwd again
  headFwd();
  delay(50);

  return;
}

/**
 * Turn head in front direction
 */
void headFwd() {
  myServo.write(110);
}

/**
 * Turn head to the left side
 */
void headLeft() {
  myServo.write(160);
}

/**
 * Turn head to the right side
 */
void headRight() {
  myServo.write(60);
}

/**
 * This function reads a PING))) ultrasonic rangefinder and returns the
 * distance to the closest object in range. To do this, it sends a pulse
 * to the sensor to initiate a reading, then listens for a pulse
 * to return.  The length of the returning pulse is proportional to
 * the distance of the object from the sensor.
 */
int getDistance() {
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse.
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  // Duration until HIGH pulse is received. Timeout is set to 1740 microseconds which equals to 30 cm distance.
  // That means that objects farther then 30 cm are not detected.
  //int duration = pulseIn(pingPin, HIGH, 1740);
  int duration = pulseIn(pingPin, HIGH, 5000); // experimental timeout

  int distance = microsecondsToCentimeters(duration);
  if (distance == 0) {
    // Convert zero value reading from distance, because zero means no obsticles within sensor range
    distance = 99;
  }
  
  return distance;
}

/**
 * Converts time to distance for ultrasonic sensor
 */
long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

/**
 * Decide in which direction to go next depending on measured distances
 * from front, left and right side.
 * 
 * TODO: explain logic..
 */
int decideDirection(int distances[3]) {
  int distFwd = distances[0];
  int distLeft = distances[1];
  int distRight = distances[2];
  
  if (distFwd > DISTANCE_MIN && distLeft > DISTANCE_MIN && distRight > DISTANCE_MIN) {
    // If no obsticles are found, set direction forward
    Serial.print("-DIRECTION_FWD");
    Serial.println();
    return DIRECTION_FWD;
  }

  if (distFwd <= DISTANCE_MIN && distLeft > DISTANCE_MIN && distRight > DISTANCE_MIN) {
    // If obsticle is only in front, choose randomly to go left or right
    int rndNum = random(0,100);
    if (rndNum < 50) {
      Serial.print("-DIRECTION_LEFT_90");
      Serial.println();
      return DIRECTION_LEFT_90;
    } else {
      Serial.print("-DIRECTION_RIGHT_90");
      Serial.println();
      return DIRECTION_RIGHT_90;
    }
  }

  if (distFwd <= DISTANCE_MIN && distLeft <= DISTANCE_MIN && distRight > DISTANCE_MIN) {
    // If obsticles are in front and on left, set direction to right 90 degrees
    Serial.print("-DIRECTION_RIGHT_90");
    Serial.println();
    return DIRECTION_RIGHT_90;
  }

  if (distFwd <= DISTANCE_MIN && distLeft > DISTANCE_MIN && distRight <= DISTANCE_MIN) {
    // If obsticles are in front and on right, set direction to left 90 degrees
    Serial.print("-DIRECTION_LEFT_90");
    Serial.println();
    return DIRECTION_LEFT_90;
  }

  if (distFwd > DISTANCE_MIN && distLeft <= DISTANCE_MIN && distRight > DISTANCE_MIN) {
    // If obsticle in only on left, set direction to right 45 degrees
    Serial.print("-DIRECTION_RIGHT_45");
    Serial.println();
    return DIRECTION_RIGHT_45;
  }

  if (distFwd > DISTANCE_MIN && distLeft > DISTANCE_MIN && distRight <= DISTANCE_MIN) {
    // If obsticle in only on left, set direction to left 45 degrees
    Serial.print("-DIRECTION_LEFT_45");
    Serial.println();
    return DIRECTION_LEFT_45;
  }
  
  if (distFwd <= DISTANCE_MIN && distLeft <= DISTANCE_MIN && distRight <= DISTANCE_MIN) {
    // If obsticles are on each side, set direction to turn around
    Serial.print("-DIRECTION_BWD");
    Serial.println();
    return DIRECTION_BWD;
  }
}

void goDirection(int direc) {
  switch(direc) {
    case DIRECTION_FWD:
      goForward();
      break;
    case DIRECTION_RIGHT_90:
      goBackward();
      delay(200);
      goStop();
      delay(200);
      goRight();
      delay(800);
      goStop();
      break;
    case DIRECTION_LEFT_90:
      goBackward();
      delay(200);
      goStop();
      delay(200);
      goLeft();
      delay(800);
      goStop();
      break;
    case DIRECTION_RIGHT_45:
      goBackward();
      delay(200);
      goStop();
      delay(200);
      goRight();
      delay(400);
      goStop();
      break;
    case DIRECTION_LEFT_45:
      goBackward();
      delay(200);
      goStop();
      delay(200);
      goLeft();
      delay(400);
      goStop();
      break;
    case DIRECTION_BWD:
      goBackward();
      delay(300);
      goStop();
      delay(200);
      goRight();
      delay(900);
      goStop();
      break;
  }
}


void actionWonder() {
  
}

void actionDance() {
  
}


/*int decideAction() {
}*/

void goStop() {
  Serial.print("STOPPING");
  Serial.println();
  digitalWrite(LMotorFw, LOW);
  digitalWrite(LMotorBw, LOW);
  digitalWrite(RMotorFw, LOW);
  digitalWrite(RMotorBw, LOW);
  analogWrite(LMotorEn, 0);
  analogWrite(RMotorEn, 0);
  robotIsMoving = 0;
}

void goForward() {
  Serial.print("GOING FWD");
  Serial.println();
  digitalWrite(LMotorFw, HIGH);
  digitalWrite(LMotorBw, LOW);
  digitalWrite(RMotorFw, HIGH);
  digitalWrite(RMotorBw, LOW);
  analogWrite(LMotorEn, LMotorSpeed);
  analogWrite(RMotorEn, RMotorSpeed);
  robotIsMoving = 1;
}

void goRight() {
  digitalWrite(LMotorFw, HIGH);
  digitalWrite(LMotorBw, LOW);
  digitalWrite(RMotorFw, LOW);
  digitalWrite(RMotorBw, HIGH);
  analogWrite(LMotorEn, LMotorSpeed);
  analogWrite(RMotorEn, RMotorSpeed);
  robotIsMoving = 1;
}

void goLeft() {
  digitalWrite(LMotorFw, LOW);
  digitalWrite(LMotorBw, HIGH);
  digitalWrite(RMotorFw, HIGH);
  digitalWrite(RMotorBw, LOW);
  analogWrite(LMotorEn, LMotorSpeed);
  analogWrite(RMotorEn, RMotorSpeed);
  robotIsMoving = 1;
}

void goBackward() {
  Serial.print("GOING FWD");
  Serial.println();
  digitalWrite(LMotorFw, LOW);
  digitalWrite(LMotorBw, HIGH);
  digitalWrite(RMotorFw, LOW);
  digitalWrite(RMotorBw, HIGH);
  analogWrite(LMotorEn, LMotorSpeed);
  analogWrite(RMotorEn, RMotorSpeed);
  robotIsMoving = 1;
}
